public class AutoShop {

    public static void main(String[] args){

        Sedan sedan1 = new Sedan(200,120000, "black", 3800);
        Sedan sedan2 = new Sedan(240, 140000, "silver", 4600);

        Truck truck1 = new Truck(140, 300000, "red", 3800);

        Hatchback hatchback1 = new Hatchback(170,80000,"blue", 2018);


        System.out.println(sedan1 + " finall price: " + sedan1.getSalePrice());
        System.out.println(sedan2 + " finall price: " + sedan2.getSalePrice());
        System.out.println(truck1 + " finall price: " +  truck1.getSalePrice());
        System.out.println(hatchback1 + "finall price: " + hatchback1.getSalePrice());
    }
}
