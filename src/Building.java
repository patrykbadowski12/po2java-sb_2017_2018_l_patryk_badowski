

public class Building extends Location {
	private static int ID=1;

	public Building( ) {
		id.buildingNumber = ID++;
	}

	public void description() {
		System.out.println("Budynek nr: " + id.buildingNumber + " ma "
				+ id.floorNumber.size() + " pietra i " + id.roomNumber.size()
				+ " pokoi.");
	}
}