
public class Invoice {
	
	private static int ID=0;
	private int itemID;
	private String itemDescription;
	private int quantity;
	private double price;
	
	public Invoice(String itemDescription, int quantity, double price) {
		this.itemDescription = itemDescription;
		this.quantity = quantity;
		this.price = price;
		itemID=ID++;
		
	}

	public int getItemID() {
		return itemID;
	}

	public void setItemID(int itemID) {
		itemID = itemID;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getInvoiceAmount() {
		return quantity*price;
	}

	@Override
	public String toString() {
		return itemID +" You order a " + itemDescription + " in an amount " + quantity + " in price " + price + " = " + getInvoiceAmount();
	}

}
