public class Truck extends Car{
    public double weight;

    public Truck(double speed, double price, String color, double weight) {
        super(speed, price, color);
        this.weight = weight;
    }

    @Override
    public double getSalePrice() {
        if (this.weight > 3000) return this.price-(this.price*0.1);
        return this.price;
    }

    @Override
    public String toString() {
        return "Truck{" +
                "weight=" + weight +
                ", speed=" + speed +
                ", price=" + price +
                ", color='" + color + '\'' +
                '}';
    }
}
