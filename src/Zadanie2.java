
public class Zadanie2 {


	public static void main(String[] args) {
		if(args.length < 3) {
			System.out.println("Poda�e� za ma�� liczb� argument�w");
			return;
		}
		try {
			
			int i = Integer.parseInt(args[1]);
			int j = Integer.parseInt(args[2]);
			System.out.println(args[0].substring(i, j + 1));
		} 
		
		catch(NumberFormatException e) {
			System.out.println("Nie poda�e� liczb:" + e.getLocalizedMessage());
		} 
		
		catch(StringIndexOutOfBoundsException e) {
			System.out.println("Nieprawid�owe indeksy: " + e.getLocalizedMessage());
		}
	}
}
