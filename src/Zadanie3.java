import java.io.FileWriter;

public class Zadanie3 {

	public static void main(String[] args) {
		int a = 0, b = 0, c = 0;
		if (args.length < 4) {
			System.out.println("Za ma�o parametr�w");
			return;
		}
		try {

			a = Integer.parseInt(args[0]);
			b = Integer.parseInt(args[1]);
			c = Integer.parseInt(args[2]);

			rownianieKwadratowe(a, b, c);
		} catch (NumberFormatException e) {
            System.out.println("Z�y format liczby: " + e.getLocalizedMessage());
        }
        saveResult(args[3], rownianieKwadratowe(a,b,c));

	}


    public static void saveResult(String name, String result) {
        try {
            FileWriter fileWriter = new FileWriter(  name+ ".txt");
            fileWriter.write(result);
            fileWriter.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


        public static String rownianieKwadratowe (int a, int b, int c) {
		if (a == 0) {
		return "To nie jest r�wnanie kwadratowe.";
		}
		int d = b * b - 4 * a * c;
		if (d < 0) {
			return "Brak pierwiastk�w rzeczywistych.";
		} else if (d == 0) {
			int x = -b / (2 * a);
			return "Jeden pierwiastek: " + x;
		} else {
			double sd = Math.sqrt(d);
			double x1 = (-b - sd) / (2 * a);
			double x2 = (-b + sd) / (2 * a);
			return "Dwa pierwiastki: " + x1 + " i " + x2;
		}
	}
}


