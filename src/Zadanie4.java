import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Zadanie4 {
	public static void main(String args[]) {

		String exit = "";
		double fullInvoice = 0;
		
		String itemDescription;
		int quantity;
		double price;
		
		Scanner scanner = new Scanner(System.in);
		
		List<Invoice> listOfProducts = new ArrayList<Invoice>();

		do {
			System.out.println("Name of Products");
			itemDescription = scanner.next();
			if(itemDescription.equals("end") ) {
				break;
			}
			System.out.println("Quantity:");
			quantity = scanner.nextInt();
			System.out.println("Price:");
			price = scanner.nextDouble();
			
			listOfProducts.add(new Invoice(itemDescription, quantity, price));
			
		}while(!(exit.equalsIgnoreCase("end")));
		scanner.close();
		
		for(Invoice product: listOfProducts) {
			System.out.println(product);
			fullInvoice += product.getInvoiceAmount();
		}
		System.out.println("full invoice = " + fullInvoice);
	
	}
	

}

