import java.util.Scanner;
import java.io.FileWriter;

class WektorRoznejDlugosciException extends Exception
{
	int l1,l2;
	public WektorRoznejDlugosciException(int a,int b)
	{
		l1=a;
		l2=b;
	}
	public String getMessage() //funkcja
	{
		StringBuilder stringBuilder=new StringBuilder(); //w??czenie funkcji stringbulider
		stringBuilder.append("\nWektory r�?nej d?�go?ci.\nWektor1.length=").append(l1).append("\nWektor2.length=").append(l2); //Do??cza ci?g reprezentuj?cy warto?? logiczn? okre?lonego dla tego wyst?pienia, tworzy napis
		return stringBuilder.toString(); //metoda zwraca ci?g reprezentuj?cy dane w tej sekwencji
	}
}
class Zadanie5
{
	public static void main(String[] args)
	{
		boolean success=false; //Boolean typ zmiennej przechowuj?cy warto?? logiczn? TruE lub False
		int[][] wektor=new int[2][]; //tablica 2 wymiarowa
		String[] wektory=new String[2];// tablica
		Scanner scanner=new Scanner(System.in); //obiekt do odebrania danych od u?ytkownika
		do
		{
			System.out.println("Podaj pierwszy wektor(liczby odziel ','):");
			wektory[0]=scanner.nextLine(); //pobiera23 linijk?
			System.out.println("Podaj drugi wektor:");
			wektory[1]=scanner.nextLine();
			wektor[0]=stringNaInt(wektory[0]); //zamiana ze string na int
			wektor[1]=stringNaInt(wektory[1]);
			System.out.println("Wektor 1: "+ wektorDoWyswietlenia(wektor[0]));
			System.out.println("Wektor 2: "+ wektorDoWyswietlenia(wektor[1]));
			try
			{
				if(wektor[0].length!=wektor[1].length) throw new WektorRoznejDlugosciException(wektor[0].length,wektor[1].length); //czy wektory s? takie same
				//if(wektor[0].length!=wektor[1].length) throw new Exception("Wektory r�?nej d?ugosci.");
				success=true;
			}
			catch(Exception e) //b??d
			{
				System.out.println(e.getMessage()); //zapisuje ?a?cuch znak�w do standardowego strumienia wyj?ciowego
			}
			if(success)
			{
				int[] wektorDoZapisu=new int[wektor[0].length];
				for(int i=0;i<wektor[0].length;i++)
					wektorDoZapisu[i]=wektor[0][i]+wektor[1][i];
				try
				{
					FileWriter fileWriter=new FileWriter("odp.txt"); //zapis do pliku tekstowego.
					fileWriter.write(wektorDoWyswietlenia(wektorDoZapisu));
					fileWriter.close();
				}
				catch(Exception e)
				{
					System.out.println(e.getMessage());
				}
			}
		}while(!success);
	}

	private static int[] stringNaInt(String wektor) //deklracja metody
	{
		int[] returnValue=new int[0];
		int lastInt=0;
		String[] splitedWektor=wektor.split(",");	//rozdziela znaki i zwraca tablice znak�w
		for(int i=0;i<splitedWektor.length;i++)
		{
			int[] temp=new int[lastInt+1];
			for(int j=0;j<returnValue.length;j++)
				temp[j]=returnValue[j];
			try
			{
				temp[lastInt]=Integer.parseInt(splitedWektor[i]); //zmiana danych na int
				lastInt++;
				returnValue=temp;

			}catch(Exception e){};

		}
		return returnValue;
	}

	private static String wektorDoWyswietlenia(int[] wektor)
	{
		StringBuilder stringBuilder=new StringBuilder();
		stringBuilder.append("[ "); //Do??cza ci?g reprezentuj?cy warto?? logiczn? okre?lonego dla tego wyst?pienia, dodaje nawiasy do napisu
		for(int i=0;i<wektor.length;i++)
			stringBuilder.append(wektor[i]+" ");
		stringBuilder.append("]");
		return stringBuilder.toString();
	}
}
