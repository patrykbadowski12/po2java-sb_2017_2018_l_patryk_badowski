public class Zadanie6 {

    public static void main(String [] args){

        SavingsAccount person1 = new SavingsAccount(4212);
        SavingsAccount person2 = new SavingsAccount(123213);
        SavingsAccount person3 = new SavingsAccount(34232123);
        SavingsAccount person4 = new SavingsAccount(231);

        person1.payment(2321);
        System.out.println(person1.getAccountBalance());

        person1.calculateMonthlyInterest();
        System.out.printf("%.2f\n",person1.getAccountBalance());

        person2.payment(232);
        person2.paycheck(5232);

        SavingsAccount.setAnnuallInterestRate(0.9);

        person1.calculateMonthlyInterest();
        System.out.printf("%.2f\n",person1.getAccountBalance());
        System.out.println(SavingsAccount.annuallInterestRate);
    }
}
